module Whales
  module Part1
    class CrabAligner
      def initialize(positions:)
        @positions = positions
      end

      def call
        @positions.sum { |pos| [pos, median].max - [pos, median].min }
      end

      private

      def median
        @median ||= @positions.sort[@positions.count / 2]
      end
    end
  end
end
