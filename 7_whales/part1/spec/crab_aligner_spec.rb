require_relative '../lib/crab_aligner'

RSpec.describe Whales::Part1::CrabAligner do
  it 'aligns crab at cheapest position' do
    positions = InputFormatter::Whales.new(fixture: '../7_whales/part1/spec/fixtures/input_1').call
    crab_aligner = described_class.new(positions: positions)

    fuel_cost = crab_aligner.call

    expect(fuel_cost).to eq(37)
  end

  it 'aligns crab at cheapest position' do
    positions = InputFormatter::Whales.new(fixture: '../7_whales/part1/spec/fixtures/input_2').call
    crab_aligner = described_class.new(positions: positions)

    fuel_cost = crab_aligner.call

    expect(fuel_cost).to eq(343605)
  end
end
