require_relative '../lib/crab_aligner'

RSpec.describe Whales::Part2::CrabAligner do
  it 'aligns crab at cheapest position' do
    positions = InputFormatter::Whales.new(fixture: '../7_whales/part2/spec/fixtures/input_1').call
    crab_aligner = described_class.new(positions: positions)

    fuel_cost = crab_aligner.call

    expect(fuel_cost).to eq(168)
  end

  it 'aligns crab at cheapest position' do
    positions = InputFormatter::Whales.new(fixture: '../7_whales/part2/spec/fixtures/input_2').call
    crab_aligner = described_class.new(positions: positions)

    fuel_cost = crab_aligner.call

    expect(fuel_cost).to eq(96744904)
  end
end
