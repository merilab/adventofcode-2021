module Whales
  module Part2
    class CrabAligner
      def initialize(positions:)
        @positions = positions
      end

      def call
        (0..@positions.sort.last).to_a.map do |align_point|
          @positions.sum {|pos| fuel_cost(pos, align_point)}
        end.min
      end

      private

      def fuel_cost(from, to)
        difference = ([from, to].max - [from, to].min)
        ((1 + difference.to_f) / 2) * difference # Gauss sum
      end
    end
  end
end
