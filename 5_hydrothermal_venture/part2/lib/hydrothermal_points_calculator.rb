require_relative './line'
require_relative './overlaps_counter'

module HydrothermalVenture
  module Part2
    class HydrothermalPointsCalculator
      def initialize(lines:)
        @lines = lines
        @overlaps_counter = OverlapsCounter.new
      end

      def call
        @lines.each { |l| count_overlaps(Line.new(l).expand) }
        @overlaps_counter.result
      end

      private

      def count_overlaps(points)
        points.each { |point| @overlaps_counter.count(point) }
      end
    end
  end
end
