module HydrothermalVenture
  module Part2
    class Line
      attr_accessor :x0, :x1, :y0, :y1

      def initialize(raw_line)
        @raw_line = raw_line

        @x0 = extract_x(line_start)
        @x1 = extract_x(line_end)
        @y0 = extract_y(line_start)
        @y1 = extract_y(line_end)
      end

      def expand
        h = Horizontal.new(@raw_line).points
        v = Vertical.new(@raw_line).points
        d = Diagonal.new(@raw_line).points

        h.concat(v).concat(d)
      end

      private

      def extract_x(coords)
        coords.first.to_i
      end

      def extract_y(coords)
        coords.last.to_i
      end

      def line_end
        sanitize(@raw_line.last)
      end

      def line_start
        sanitize(@raw_line.first)
      end

      def points_range(first, last)
        first > last ? (last..first).to_a.reverse : (first..last).to_a
      end

      def sanitize(tuple)
        tuple.split(',')
      end
    end

    class Diagonal < Line
      def points
        if applies?
          range_x = points_range(x0, x1)
          range_y = points_range(y0, y1)

          range_x.each_with_index.map do |x, i|
            [range_x[i], range_y[i]]
          end
        else
          []
        end
      end

      private

      def applies?
        x0 != x1 && y0 != y1
      end
    end

    class Horizontal < Line
      def points
        return points_range(y0, y1).map { |e| [x0, e] } if applies?
        []
      end

      private

      def applies?
        x0 == x1
      end
    end

    class Vertical < Line
      def points
        return points_range(x0, x1).map { |e| [e, y0] } if applies?
        []
      end

      private

      def applies?
        y0 == y1
      end
    end
  end
end
