module HydrothermalVenture
  module Part1
    class Line
      attr_accessor :x0, :x1, :y0, :y1

      def initialize(raw_line)
        @raw_line = raw_line

        @x0 = extract_x(line_start)
        @x1 = extract_x(line_end)
        @y0 = extract_y(line_start)
        @y1 = extract_y(line_end)
      end

      def expand
        horizontal_points.concat(vertical_points)
      end

      private

      def extract_x(coords)
        coords.first.to_i
      end

      def extract_y(coords)
        coords.last.to_i
      end

      def horizontal?
        @x0 == @x1
      end

      def horizontal_points
        horizontal? ? points_range(@y0, @y1).map { |e| [@x0, e] } : []
      end

      def line_end
        sanitize(@raw_line.last)
      end

      def line_start
        sanitize(@raw_line.first)
      end

      def points_range(first, last)
        ([first, last].min..[first, last].max).to_a
      end

      def sanitize(tuple)
        tuple.split(',')
      end

      def vertical?
        @y0 == @y1
      end

      def vertical_points
        vertical? ? points_range(@x0, @x1).map { |e| [e, @y0] } : []
      end
    end
  end
end
