module HydrothermalVenture
  module Part1
    class OverlapsCounter
      def initialize
        @counter = {}
        @overlaps = {}
      end

      def count(point)
        @counter[point] = @counter[point].nil? ? 0 : @counter[point] += 1
        @overlaps[point] = true if @counter[point] > 0
      end

      def result
        @overlaps.keys.count
      end
    end
  end
end
