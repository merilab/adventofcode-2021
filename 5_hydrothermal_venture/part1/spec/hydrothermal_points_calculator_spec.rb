require_relative '../lib/hydrothermal_points_calculator'

RSpec.describe HydrothermalVenture::Part1::HydrothermalPointsCalculator do
  it 'calculates score of loser board' do
    nerby_lines = InputFormatter::HydrothermalLines.new(fixture: '../5_hydrothermal_venture/part1/spec/fixtures/input_1').call
    hydrothermal_points_calculator = described_class.new(lines: nerby_lines)

    hydrothermal_points = hydrothermal_points_calculator.call

    expect(hydrothermal_points).to eq(5)
  end

  it 'calculates score of loser board' do
    nerby_lines = InputFormatter::HydrothermalLines.new(fixture: '../5_hydrothermal_venture/part1/spec/fixtures/input_2').call
    hydrothermal_points_calculator = described_class.new(lines: nerby_lines)

    hydrothermal_points = hydrothermal_points_calculator.call

    expect(hydrothermal_points).to eq(5084)
  end
end
