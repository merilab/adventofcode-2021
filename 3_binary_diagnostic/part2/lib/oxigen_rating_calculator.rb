require_relative './base_rating_calculator'

module BinaryDiagnostic
  module Part2
    class OxigenRatingCalculator < BaseRatingCalculator
      DEFAULT_BIT_COMPARER = '1'

      def common_bit(ocurrences)
        ocurrences.key(ocurrences.values.max)
      end

      def default_common_bit
        DEFAULT_BIT_COMPARER
      end
    end
  end
end
