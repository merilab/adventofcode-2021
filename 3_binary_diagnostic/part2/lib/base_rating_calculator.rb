module BinaryDiagnostic
  module Part2
    class BaseRatingCalculator
      def initialize(diagnostic_report:)
        @diagnostic_report = diagnostic_report
      end

      def call
        calculate(@diagnostic_report, 0).first
      end

      private

      def calculate(numbers, position)
        while numbers.count > 1
          comparer = find_common_bit(numbers, position)
          numbers = numbers.select { |n| n.chars[position] == comparer }
          position += 1
          calculate(numbers, position)
        end

        numbers
      end

      def equally_common?(ocurrences)
        ocurrences.values.uniq.size == 1
      end

      def find_common_bit(numbers, position)
        bits_in_position = numbers.map { |n| n.chars[position] }
        ocurrences_per_bit = bits_in_position.tally

        equally_common?(ocurrences_per_bit) ? default_common_bit : common_bit(ocurrences_per_bit)
      end
    end
  end
end
