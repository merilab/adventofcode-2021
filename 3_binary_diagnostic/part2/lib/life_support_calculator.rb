module BinaryDiagnostic
  module Part2
    class LifeSupportCalculator
      def initialize(co2:, oxigen:)
        @co2 = co2
        @oxigen = oxigen
      end

      def call
        to_decimal(@co2) * to_decimal(@oxigen)
      end

      private

      def to_decimal(value)
        value.to_i(2)
      end
    end
  end
end
