require_relative './base_rating_calculator'

module BinaryDiagnostic
  module Part2
    class Co2RatingCalculator < BaseRatingCalculator
      DEFAULT_BIT_COMPARER = '0'

      def common_bit(ocurrences)
        ocurrences.key(ocurrences.values.min)
      end

      def default_common_bit
        DEFAULT_BIT_COMPARER
      end
    end
  end
end
