require_relative '../lib/life_support_calculator'

RSpec.describe BinaryDiagnostic::Part2::LifeSupportCalculator do
  it 'calculates life support rating' do
    calculator = described_class.new(
      co2: '01010',
      oxigen: '10111'
    )

    rate = calculator.call

    expect(rate).to eq(230)
  end

  it 'calculates life support rating' do
    calculator = described_class.new(
      co2: '001010100001',
      oxigen: '100101011101'
    )

    rate = calculator.call

    expect(rate).to eq(1613181)
  end
end
