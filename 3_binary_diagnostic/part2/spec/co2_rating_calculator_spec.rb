require_relative '../lib/co2_rating_calculator'

RSpec.describe BinaryDiagnostic::Part2::Co2RatingCalculator do
  it 'calculates co2 rating' do
    diagnostic_report = InputFormatter::Diagnosis.new(fixture: '../3_binary_diagnostic/part2/spec/fixtures/input_1').call
    calculator = described_class.new(diagnostic_report: diagnostic_report)

    rate = calculator.call

    expect(rate).to eq('01010')
  end

  it 'calculates gamma rate based on the most common bit in the diagnostic report' do
    diagnostic_report = InputFormatter::Diagnosis.new(fixture: '../3_binary_diagnostic/part2/spec/fixtures/input_2').call
    calculator = described_class.new(diagnostic_report: diagnostic_report)

    rate = calculator.call

    expect(rate).to eq('001010100001')
  end
end
