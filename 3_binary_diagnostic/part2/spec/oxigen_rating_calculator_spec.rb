require_relative '../lib/oxigen_rating_calculator'

RSpec.describe BinaryDiagnostic::Part2::OxigenRatingCalculator do
  it 'calculates oxigen rating' do
    diagnostic_report = InputFormatter::Diagnosis.new(fixture: '../3_binary_diagnostic/part2/spec/fixtures/input_1').call
    calculator = described_class.new(diagnostic_report: diagnostic_report)

    rate = calculator.call

    expect(rate).to eq('10111')
  end

  it 'calculates oxigen rating' do
    diagnostic_report = InputFormatter::Diagnosis.new(fixture: '../3_binary_diagnostic/part2/spec/fixtures/input_2').call
    calculator = described_class.new(diagnostic_report: diagnostic_report)

    rate = calculator.call

    expect(rate).to eq('100101011101')
  end
end
