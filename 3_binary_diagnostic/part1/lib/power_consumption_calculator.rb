module BinaryDiagnostic
  module Part1
    class PowerConsumptionCalculator
      def initialize(epsilon:, gamma:)
        @epsilon = epsilon
        @gamma = gamma
      end

      def call
        to_decimal(@epsilon) * to_decimal(@gamma)
      end

      private

      def to_decimal(value)
        value.to_i(2)
      end
    end
  end
end
