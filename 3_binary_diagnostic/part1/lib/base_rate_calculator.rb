module BinaryDiagnostic
  module Part1
    class BaseRateCalculator
      def initialize(diagnostic_report:)
        @diagnostic_report = diagnostic_report
      end

      def call
        rate = ''

        positions.times do |i|
          bits_in_position = @diagnostic_report.map { |number| number.chars[i] }

          ocurrences_per_bit = bits_in_position.tally
          least_common_bit = ocurrences_per_bit.key(common_bit(ocurrences_per_bit))

          rate.concat(least_common_bit)
        end

        rate
      end

      private

      def positions
        @diagnostic_report.first.chars.count
      end
    end
  end
end
