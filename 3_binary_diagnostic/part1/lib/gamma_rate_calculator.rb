require_relative './base_rate_calculator'

module BinaryDiagnostic
  module Part1
    class GammaRateCalculator < BaseRateCalculator
      private

      def common_bit(ocurrences)
        ocurrences.values.max
      end
    end
  end
end
