require_relative './base_rate_calculator'

module BinaryDiagnostic
  module Part1
    class EpsilonRateCalculator < BaseRateCalculator
      private

      def common_bit(ocurrences)
        ocurrences.values.min
      end
    end
  end
end
