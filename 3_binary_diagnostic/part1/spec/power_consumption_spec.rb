require_relative '../lib/power_consumption_calculator'

RSpec.describe BinaryDiagnostic::Part1::PowerConsumptionCalculator do
  it 'calculates power consumption' do
    calculator = described_class.new(
      epsilon: '01001',
      gamma: '10110'
    )

    rate = calculator.call

    expect(rate).to eq(198)
  end

  it 'calculates power consumption' do
    calculator = described_class.new(
      epsilon: '011000011100',
      gamma: '100111100011'
    )

    rate = calculator.call

    expect(rate).to eq(3958484)
  end
end
