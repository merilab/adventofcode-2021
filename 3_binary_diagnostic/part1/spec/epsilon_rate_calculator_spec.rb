require_relative '../lib/epsilon_rate_calculator'

RSpec.describe BinaryDiagnostic::Part1::EpsilonRateCalculator do
  it 'calculates epsilon rate based on the least common bit in the diagnostic report' do
    diagnostic_report = InputFormatter::Diagnosis.new(fixture: '../3_binary_diagnostic/part1/spec/fixtures/input_1').call
    calculator = described_class.new(diagnostic_report: diagnostic_report)

    rate = calculator.call

    expect(rate).to eq('01001')
  end

  it 'calculates epsilon rate based on the least common bit in the diagnostic report' do
    diagnostic_report = InputFormatter::Diagnosis.new(fixture: '../3_binary_diagnostic/part1/spec/fixtures/input_2').call
    calculator = described_class.new(diagnostic_report: diagnostic_report)

    rate = calculator.call

    expect(rate).to eq('011000011100')
  end
end
