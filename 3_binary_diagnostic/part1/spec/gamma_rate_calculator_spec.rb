require_relative '../lib/gamma_rate_calculator'

RSpec.describe BinaryDiagnostic::Part1::GammaRateCalculator do
  it 'calculates gamma rate based on the most common bit in the diagnostic report' do
    diagnostic_report = InputFormatter::Diagnosis.new(fixture: '../3_binary_diagnostic/part1/spec/fixtures/input_1').call
    calculator = described_class.new(diagnostic_report: diagnostic_report)

    rate = calculator.call

    expect(rate).to eq('10110')
  end

  it 'calculates gamma rate based on the most common bit in the diagnostic report' do
    diagnostic_report = InputFormatter::Diagnosis.new(fixture: '../3_binary_diagnostic/part1/spec/fixtures/input_2').call
    calculator = described_class.new(diagnostic_report: diagnostic_report)

    rate = calculator.call

    expect(rate).to eq('100111100011')
  end
end
