module LanternFish
  module Part2
    class LanternFishCalculator
      NEWBORN_STATUS = 9
      RESET_STATUS = 6
      INITIAL_FISH_COUNT = 0

      def initialize(internal_counters:)
        @status_matrix = Array.new(NEWBORN_STATUS, INITIAL_FISH_COUNT)

        internal_counters.each do |status|
          @status_matrix[status] += 1
        end
      end

      def call(iterations:)
        iterations.times do
          completed_cycles = @status_matrix.shift
          @status_matrix[RESET_STATUS] += completed_cycles
          @status_matrix << completed_cycles
        end

        @status_matrix.sum
      end
    end
  end
end
