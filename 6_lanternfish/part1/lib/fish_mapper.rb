require_relative './fish'

module LanternFish
  module Part1
    class FishMapper
      def self.call(internal_counters:)
        internal_counters.map { |days_left| Fish.new(days_left: days_left) }
      end
    end
  end
end
