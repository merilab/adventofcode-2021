require_relative './fish_mapper'
require_relative './fish'

module LanternFish
  module Part1
    class LanternFishCalculator
      def initialize(internal_counters:)
        @fish_matrix = FishMapper.call(internal_counters: internal_counters)
      end

      def call(iterations:)
        iterations.times do
          @fish_matrix.each do |fish|
            @fish_matrix << Fish.new if fish.reproduced?
          end
        end

        @fish_matrix.count
      end
    end
  end
end
