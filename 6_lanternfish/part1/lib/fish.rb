module LanternFish
  module Part1
    class Fish
      RESET_STATUS = 6
      NEWBORN_STATUS = 9

      def initialize(days_left: nil)
        @days_left = days_left || NEWBORN_STATUS
      end

      def reproduced?
        @days_left -= 1

        return reproduced if complete?
      end

      private

      def complete?
        @days_left == -1
      end

      def reproduced
        @days_left = RESET_STATUS
        true
      end
    end
  end
end
