require_relative '../lib/lanternfish_calculator'

RSpec.describe LanternFish::Part1::LanternFishCalculator do
  it 'counts lanternfishes after 18 days' do
    internal_counters = InputFormatter::LanternFish.new(fixture: '../6_lanternfish/part1/spec/fixtures/input_1').call
    lanternfish_calculator = described_class.new(internal_counters: internal_counters)

    fish_count = lanternfish_calculator.call(iterations: 18)

    expect(fish_count).to eq(26)
  end

  it 'counts lanternfishes after 80 days' do
    internal_counters = InputFormatter::LanternFish.new(fixture: '../6_lanternfish/part1/spec/fixtures/input_1').call
    lanternfish_calculator = described_class.new(internal_counters: internal_counters)

    fish_count = lanternfish_calculator.call(iterations: 80)

    expect(fish_count).to eq(5934)
  end

  it 'counts lanternfishes after 80 days' do
    internal_counters = InputFormatter::LanternFish.new(fixture: '../6_lanternfish/part1/spec/fixtures/input_2').call
    lanternfish_calculator = described_class.new(internal_counters: internal_counters)

    fish_count = lanternfish_calculator.call(iterations: 80)

    expect(fish_count).to eq(362346)
  end
end
