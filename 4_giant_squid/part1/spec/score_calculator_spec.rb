require_relative '../lib/score_calculator'

RSpec.describe GiantSquid::Part1::ScoreCalculator do
  it 'calculates score of winner board' do
    bingo_data = InputFormatter::Bingo.new(fixture: '../4_giant_squid/part1/spec/fixtures/input_1').call
    score_calculator = described_class.new(numbers: bingo_data[:numbers], boards: bingo_data[:boards])

    score = score_calculator.call

    expect(score).to eq(4512)
  end

  it 'calculates score of winner board' do
    bingo_data = InputFormatter::Bingo.new(fixture: '../4_giant_squid/part1/spec/fixtures/input_2').call
    score_calculator = described_class.new(numbers: bingo_data[:numbers], boards: bingo_data[:boards])

    score = score_calculator.call

    expect(score).to eq(10374)
  end
end
