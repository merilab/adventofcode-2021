require_relative './board'

module GiantSquid
  module Part1
    class ScoreCalculator
      def initialize(numbers:, boards:)
        @boards = boards.map { |b| Part1::Board.new(origin_board: b) }
        @numbers = numbers
        @numbers_called = []
      end

      def call
        first_solved_board = play_to_win

        unmarked_numbers_total(first_solved_board) * @numbers_called.last
      end

      private

      def find_in_row(board, number)
        board.rows.find do |row|
          row.mark(number)
          row.complete?
        end
      end

      def mark(number)
        @numbers_called << number
        @boards.find { |board| find_in_row(board, number) }
      end

      def play_to_win
        first_solved_board = nil

        @numbers.find { |number| first_solved_board = mark(number) }

        first_solved_board
      end

      def unmarked_numbers_total(board)
        (board.list - @numbers_called).sum
      end
    end
  end
end
