module GiantSquid
  module Part1
    class Board
      attr_accessor :list, :rows

      def initialize(origin_board:)
        @line_size = origin_board.first.count
        @list = origin_board.flatten.map(&:to_i)
        @rows = origin_board.map { |r| Line.new(r) }
      end
    end

    class Line
      attr_accessor :numbers, :matches, :size

      def initialize(origin_line)
        @matches = 0
        @numbers = origin_line.map(&:to_i)
        @size = origin_line.size
      end

      def complete?
        @matches >= @size
      end

      def mark(number)
        @matches += @numbers.grep(number).count
      end
    end
  end
end
