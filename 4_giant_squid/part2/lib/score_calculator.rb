require_relative './board'

module GiantSquid
  module Part2
    class ScoreCalculator
      def initialize(numbers:, boards:)
        @boards = boards.map { |b| Part2::Board.new(origin_board: b) }
        @numbers = numbers
        @numbers_called = []
      end

      def call
        last_solved_board = play_to_lose(@boards)

        unmarked_numbers_total(last_solved_board) * @numbers_called.last
      end

      private

      def board_solved?(board, number)
        mark(board.rows, number) || mark(board.columns, number)
      end

      def mark(board, number)
        board.find do |line|
          line.mark(number)
          line.complete?
        end
      end

      def play_to_lose(unsolved_boards)
        last_solved_board = nil

        @numbers.find do |number|
          @numbers_called << number

          unsolved_boards.reject! { |board| board_solved?(board, number) }

          last_solved_board = unsolved_boards.last if unsolved_boards.count == 1
          unsolved_boards.count == 0
        end

        last_solved_board
      end

      def unmarked_numbers_total(board)
        (board.list - @numbers_called).sum
      end
    end
  end
end
