module GiantSquid
  module Part2
    class Board
      attr_accessor :columns, :list, :rows

      def initialize(origin_board:)
        @line_size = origin_board.first.count
        @list = origin_board.flatten.map(&:to_i)
        @columns = map_columns(origin_board)
        @rows = map_rows(origin_board)
      end

      private

      def map_columns(board)
        board.each_with_index.map { |_, i| Line.new(board.map { |r| r[i] }) }
      end

      def map_rows(board)
        board.map { |r| Line.new(r) }
      end
    end

    class Line
      attr_accessor :numbers, :matches, :size

      def initialize(origin_line)
        @matches = 0
        @numbers = origin_line.map(&:to_i)
        @size = origin_line.size
      end

      def complete?
        @matches >= @size
      end

      def mark(number)
        @matches += @numbers.grep(number).count
      end
    end
  end
end
