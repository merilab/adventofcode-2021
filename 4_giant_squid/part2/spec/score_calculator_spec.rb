require_relative '../lib/score_calculator'

RSpec.describe GiantSquid::Part2::ScoreCalculator do
  it 'calculates score of loser board' do
    bingo_data = InputFormatter::Bingo.new(fixture: '../4_giant_squid/part1/spec/fixtures/input_1').call
    score_calculator = described_class.new(numbers: bingo_data[:numbers], boards: bingo_data[:boards])

    score = score_calculator.call

    expect(score).to eq(1924)
  end

  it 'calculates score of loser board' do
    bingo_data = InputFormatter::Bingo.new(fixture: '../4_giant_squid/part1/spec/fixtures/input_2').call
    score_calculator = described_class.new(numbers: bingo_data[:numbers], boards: bingo_data[:boards])

    score = score_calculator.call

    expect(score).to eq(24742)
  end
end
