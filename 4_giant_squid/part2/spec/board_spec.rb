require_relative '../lib/board'


RSpec.describe GiantSquid::Part2::Board do
  let(:origin_board) do
    [
      ['22', '13', '17'],
      ['8', '2', '23'],
      ['21', '9', '14']
    ]
  end

  it 'stores rows' do

    board = described_class.new(origin_board: origin_board)

    expect(board.rows[0].numbers).to eq([22, 13, 17])
    expect(board.rows[1].numbers).to eq([8, 2, 23])
    expect(board.rows[2].numbers).to eq([21, 9, 14])
  end

  it 'stores columns' do

    board = described_class.new(origin_board: origin_board)

    expect(board.columns[0].numbers).to eq([22, 8, 21])
    expect(board.columns[1].numbers).to eq([13, 2, 9])
    expect(board.columns[2].numbers).to eq([17, 23, 14])
  end

  it 'lists board numbers' do
    board = described_class.new(origin_board: origin_board)

    expect(board.list).to eq([22, 13, 17, 8, 2, 23, 21, 9, 14])
  end
end
