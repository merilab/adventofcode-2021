require_relative '../lib/trend_calculator'

RSpec.describe SonarSweep::Part2::TrendCalculator do
  it 'calculates how many sums are larger than the previous sum' do
    measures = InputFormatter::Measures.new(fixture: '../1_sonar_sweep/part2/spec/fixtures/input_1').call
    calculator = described_class.new(measures: measures)

    result = calculator.call

    expect(result).to eq(5)
  end

  it 'calculates how many sums are larger than the previous sum' do
    measures = InputFormatter::Measures.new(fixture: '../1_sonar_sweep/part2/spec/fixtures/input_2').call
    calculator = described_class.new(measures: measures)

    result = calculator.call

    expect(result).to eq(1248)
  end
end
