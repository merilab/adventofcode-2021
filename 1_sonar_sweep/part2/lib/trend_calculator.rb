module SonarSweep
  module Part2
    class TrendCalculator
      PAIR_SIZE = 2
      SUM_WINDOW_SIZE = 3

      def initialize(measures:)
        @measures = measures
      end

      def call
        window_sums = @measures.each_cons(SUM_WINDOW_SIZE)
                               .collect {|window| window.sum }

        count_increasing_values(window_sums)
      end

      private

      def count_increasing_values(values)
        values.each_cons(PAIR_SIZE)
              .select { |measure_pair| measure_pair.last > measure_pair.first }
              .count
      end
    end
  end
end
