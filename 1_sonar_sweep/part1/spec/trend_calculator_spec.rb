require_relative '../lib/trend_calculator'

RSpec.describe SonarSweep::Part1::TrendCalculator do
  it 'calculates how many measurements are larger than the previous measurement' do
    measures = InputFormatter::Measures.new(fixture: '../1_sonar_sweep/part1/spec/fixtures/input_1').call
    calculator = described_class.new(measures: measures)

    result = calculator.call

    expect(result).to eq(8)
  end

  it 'calculates how many measurements are larger than the previous measurement' do
    measures = InputFormatter::Measures.new(fixture: '../1_sonar_sweep/part1/spec/fixtures/input_2').call
    calculator = described_class.new(measures: measures)

    result = calculator.call

    expect(result).to eq(1298)
  end
end
