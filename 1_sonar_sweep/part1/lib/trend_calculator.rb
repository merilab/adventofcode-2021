module SonarSweep
  module Part1
    class TrendCalculator
      PAIR_SIZE = 2

      def initialize(measures:)
        @measures = measures
      end

      def call
        @measures.each_cons(PAIR_SIZE)
                 .select { |measure_pair| measure_pair.last > measure_pair.first }
                 .count
      end
    end
  end
end
