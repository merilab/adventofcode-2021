require_relative '../lib/position_calculator'

RSpec.describe Dive::Part1::PositionCalculator do
  it 'calculates final position' do
    steps = InputFormatter::Positions.new(fixture: '../2_dive/part1/spec/fixtures/input_1').call
    calculator = described_class.new(steps: steps)

    result = calculator.call

    expect(result).to eq(150)
  end

  it 'calculates final position' do
    steps = InputFormatter::Positions.new(fixture: '../2_dive/part1/spec/fixtures/input_2').call
    calculator = described_class.new(steps: steps)

    result = calculator.call

    expect(result).to eq(1507611)
  end
end
