module Dive
  module Part1
    class PositionCalculator
      DOWN    = 'down'
      FORWARD = 'forward'
      UP      = 'up'

      def initialize(steps:)
        @steps_by_type = steps.group_by(&:first)
      end

      def call
        sum_step_values(FORWARD) * (sum_step_values(DOWN) - sum_step_values(UP))
      end

      private

      def sum_step_values(direction)
        @steps_by_type[direction].sum(&:last)
      end
    end
  end
end
