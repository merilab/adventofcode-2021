module Dive
  module Part2
    class Controls
      INITIAL_VALUE = 0

      def initialize
        @aim   = INITIAL_VALUE
        @depth = INITIAL_VALUE
        @x     = INITIAL_VALUE
      end

      def go_down(value)
        @aim += value
      end

      def go_up(value)
        @aim -= value
      end

      def move_forward(value)
        @x += value
        @depth += @aim * value
      end

      def position
        @x * @depth
      end
    end
  end
end
