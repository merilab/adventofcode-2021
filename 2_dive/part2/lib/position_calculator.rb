require_relative './controls'

module Dive
  module Part2
    class PositionCalculator
      DOWN    = 'down'
      FORWARD = 'forward'
      UP      = 'up'

      def initialize(steps:)
        @steps = steps
        @controls = Part2::Controls.new
      end

      def call
        @steps.each do |step|
          command = step.first
          value = step.last

          @controls.go_down(value)      if command == DOWN
          @controls.go_up(value)        if command == UP
          @controls.move_forward(value) if command == FORWARD
        end

        @controls.position
      end
    end
  end
end
