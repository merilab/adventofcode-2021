require_relative '../lib/position_calculator'

RSpec.describe Dive::Part2::PositionCalculator do
  it 'calculates final position' do
    steps = InputFormatter::Positions.new(fixture: '../2_dive/part2/spec/fixtures/input_1').call
    calculator = described_class.new(steps: steps)

    result = calculator.call

    expect(result).to eq(900)
  end

  it 'calculates final position' do
    steps = InputFormatter::Positions.new(fixture: '../2_dive/part2/spec/fixtures/input_2').call
    calculator = described_class.new(steps: steps)

    result = calculator.call

    expect(result).to eq(1880593125)
  end
end
