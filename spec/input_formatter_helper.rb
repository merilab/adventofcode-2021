module InputFormatter
  class Base
    def initialize(fixture:)
      @raw_input = File.read("./spec/#{fixture}")
    end
  end

  class Measures < Base
    def call
      @raw_input.split("\n").map { |i| i.to_i }
    end
  end

  class Positions < Base
    def call
      @raw_input.split("\n").map do |i|
        step = i.split(' ')
        [step.first, step.last.to_i]
      end
    end
  end

  class Diagnosis < Base
    def call
      @raw_input.split("\n")
    end
  end

  class Bingo < Base
    BOARD_ROWS = 5

    def call
      splitted = @raw_input.split("\n")

      numbers = extract_numbers(splitted)

      splitted.shift
      raw_boards = splitted.reject {|i| i.empty? }
      raw_boards = raw_boards.map(&:split)

      start_index = 0
      final_index = start_index + BOARD_ROWS
      result_boards = []

      boards = extract_boards(raw_boards, result_boards, start_index, final_index)

      {
        numbers: numbers,
        boards: boards
      }
    end

    private

    def extract_boards(array, result, start_index, final_index)
      if final_index <= array.size
        result << array[start_index..final_index - 1]
        start_index = final_index
        final_index = start_index + BOARD_ROWS
        extract_boards(array, result, start_index, final_index)
      end

      result
    end

    def extract_numbers(array)
      array[0].split(',').map(&:to_i)
    end
  end

  class HydrothermalLines < Base
    def call
      @raw_input.split("\n").map { |raw_line| raw_line.split(' -> ') }
    end
  end

  class LanternFish < Base
    def call
      @raw_input.split(',').map(&:to_i)
    end
  end

  class Whales < Base
    def call
      @raw_input.split(',').map(&:to_i)
    end
  end
end
