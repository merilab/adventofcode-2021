# Welcome

This is the repo where I'm sharing the solutions of Advent of Code puzzle. As a Ruby expert I chose that language to solve them.

Each solution is structured as follows:
- lib/day#_puzzle_name/part#
- spec/day#_puzzle_name/part#

I used this to improve my Ruby knowledge and good practices skills. Therefore, my approach consists in:
- Using TDD for design and refactor in order to provide a solution that is easy to understand.
- Use the most efficient and idiomatic Ruby resource.

[[from adventofcode.com](https://adventofcode.com/2021/about)]

Advent of Code is an Advent calendar of small programming puzzles for a variety of skill sets and skill levels that can be solved in any programming language you like. People use them as a speed contest, interview prep, company training, university coursework, practice problems, or to challenge each other.

You don't need a computer science background to participate - just a little programming knowledge and some problem solving skills will get you pretty far. Nor do you need a fancy computer; every problem has a solution that completes in at most 15 seconds on ten-year-old hardware.

The difficulty and subject matter varies throughout each event. Very generally, the puzzles get more difficult over time, but your specific skillset will make each puzzle significantly easier or harder for you than someone else.
